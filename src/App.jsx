import React, { useState } from "react";
import "./styles.css";

function clean(obj) {
  for (const key in obj) {
    if (obj[key] === false) {
      delete obj[key];
    }
  }
  return obj;
}

function readFile(file) {
  return new Promise((resolve) => {
    const reader = new FileReader();

    reader.onload = (e) => {
      resolve(e.target.result);
    };

    reader.readAsText(file);
  });
}

function writeFile(data) {
  const json = JSON.stringify(data, null, 2);
  const encoded = `text/json;charset=utf-8,${encodeURIComponent(json)}`;
  const href = `data:${encoded}`;
  return href;
}

export default function App() {
  const [file, setFile] = useState("");
  const [data, setData] = useState({});
  const [hide, setHide] = useState(true);
  const [hidden, setHidden] = useState({});

  function filterHidden([key]) {
    return !hide || !hidden[key];
  }

  async function onFile(e) {
    const file = e.target.files[0].name;
    const json = await readFile(e.target.files[0]);
    const store = localStorage.getItem(`carbure:translation:${file}`);

    const data = JSON.parse(json);
    const hidden = JSON.parse(store) ?? {};

    setFile(file);
    setData(data);
    setHidden(clean(hidden));
  }

  async function onTranslate(e) {
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
  }

  function onHide(e) {
    const nextHidden = clean({
      ...hidden,
      [e.target.name]: !hidden[e.target.name],
    });

    setHidden(nextHidden);

    localStorage.setItem(
      `carbure:translation:${file}`,
      JSON.stringify(nextHidden)
    );
  }

  return (
    <div className="App">
      <h1>Traduction</h1>

      <section>
        <h3>Instructions</h3>
        <ul>
          <li>Importer un fichier avec le bouton ci-dessous</li>
          <li>
            Chaque groupe titre/champ éditable correspond à un texte à traduire
          </li>
          <li>Écrire la traduction dans le champ prévu à cet effet</li>
          <li>
            <b>
              Important: conserver les balises numérotées (ex: {"<0></0>"}) et
              traduire leur contenu
            </b>
          </li>
          <li>
            <b>
              Important: conserver les parties "paramétriques" (ex:{" "}
              {"{{count}}"}) telles quelles
            </b>
          </li>
          <li>
            Cliquer sur le lien "Enregistrer sous" pour enregistrer les
            modifications dans un fichier
          </li>
        </ul>
      </section>

      <div className="actions">
        <input type="file" onChange={onFile} />
        <button onClick={() => setHide(!hide)}>
          {hide ? "Montrer" : "Cacher"} les traductions terminées
        </button>
        <a href={writeFile(data)} download={file}>
          Enregistrer sous
        </a>
      </div>

      {Object.entries(data)
        .filter(filterHidden)
        .map(([key, value]) => (
          <div key={key} className="translation">
            <span>{key}</span>
            <textarea value={value} name={key} onChange={onTranslate} />
            <button name={key} onClick={onHide}>
              {hidden[key] ? "Remontrer" : "Cacher"}
            </button>
          </div>
        ))}
    </div>
  );
}
